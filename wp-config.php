<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_simplementepty' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'i&OZ}/<b+P$2)VJgpGJro~NR?eoV7r`k11]jrO)oo2`X|v-(4RwoXwz@xm91`m?_' );
define( 'SECURE_AUTH_KEY',  'E,fho ADqT%J+Bc`]w;!q6QGfL$YnLV:J~gK|b2?H{TfN1ftXPUNh5</sy[d=&<z' );
define( 'LOGGED_IN_KEY',    'BPXlZxP1JGj<4vfBui7,JZM[k0.*6DvE*_e)lgKDUY2l,uovByd#tfNUX=,Mx=WB' );
define( 'NONCE_KEY',        'I#f-<Y}Ronc#e%LfUaTzMro[`mj9q@[j3GR[[S$*IL/~uu0>Y~FoQDYwNr&](Fg ' );
define( 'AUTH_SALT',        '2u*)i#w=2.:wVZLes}Ap<]TQC2=(JS~P!lGP%DCP}{.;jlv.RD! n`ZlqYy|hCdD' );
define( 'SECURE_AUTH_SALT', 'KO]!?]5_E94|oX9`b~WKqbBL}3!{~6@:e_BaL!9EX`%YUry~&DZxG{i{7}<[(;@L' );
define( 'LOGGED_IN_SALT',   'uGJj($7qG%kbD,^H9fOfjI8r)/]A;ir0dF]zIAks@|j}aWo6<i`~iq ,{S?374b,' );
define( 'NONCE_SALT',       'AIFG.td!`lG)SB[@QVf}s|I71[bj]3~a4A!4t(z}|xuWRBM2Y `*Oh.k!/=M)B_X' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
